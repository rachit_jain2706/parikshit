<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Navigation</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!--<script type="text/javascript" src="js/vendor/autosize.min.js"></script>
  <script type="text/javascript" src="js/vendor/modernizr-2.8.3.min.js"></script>-->

  <script>



  </script>

</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  
  
  <?php
    require_once("navigation.php"); 
  ?>

  <div id="neeche">
  <p>
    
    <strong>PARIKSHIT</strong> is a student satellite project undertaken by the students of 
    <strong>Manipal Institute of Technology(MIT)</strong>
    under the guidance of <strong>Indian Space Research Organisation(ISRO)</strong>. 
    Our mission is to design, fabricate and operate a 2U nanosatellite which will click images of southern 
    India and hence successfully test our payloads.
    The payloads of our satellite are Terrestrial Thermal Imaging and Deorbit using Electrodynamic tether.
    The satellite’s lifetime is six months after which it will be deorbited by Electrodynamic tether.

  </p>
  </div>

  <?php
    require("footer.php");
  ?>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
