<?php
    echo '
    <div id="common_div">
      <header>
        <div class="navigation">
          <div class="logo">
            <a href="home.php"><img src="uploads/icon.png"></img></a>
          </div>
          <div class="drop">      
              <ul class="drop_menu" onclick="changeClass(event)">
                <li><a href="team.php" class="selected">Team</a></li>     
                <li>
                  <a href="subsystems.php">Subsystems</a>             
                  <ul>
                    <li><a href="">ADCS</a></li>
                    <li><a href="">COMMS</a></li>
                    <li><a href="">EPS</a></li>
                    <li><a href="">ODHS</a></li>
                    <li><a href="">Payload</a></li>
                    <li><a href="">STMS</a></li>
                  </ul>
                </li>
                <li><a href="about_satellite.php">About us</a></li>
                <li><a href="contact_us.php">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </div>'
  ?>