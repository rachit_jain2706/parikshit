<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Parikshit | Home</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- Hide the header on scroll -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript">
    $(window).ready(function() {
    $('#loading').hide();
});
    </script>
</head>
<body>
  <!-- Loading gif-->
  <div id="loading"></div>
  
  <?php
    require_once("header.php"); 
  ?>
  <div id="starryskydiv">
    <img src="uploads/starry_sky.jpg" id="starryskyimg">
    <img src="uploads/icon.png" id="parikshit-logo">
    <h1 id="starryskytext">The Sky Is No Limit</h1>
  </div>
  <div id="neeche1">
    <h2 class="headings">Who we are</h2>
    <div class="below_text"></div>
    <p>
      PARIKSHIT is a student satellite project undertaken by the students of 
      Manipal Institute of Technology(MIT)
      under the guidance of Indian Space Research Organisation(ISRO). 
      Our mission is to design, fabricate and operate a 2U nanosatellite which will click images of southern 
      India and hence successfully test our payloads.
      The payloads of our satellite are Terrestrial Thermal Imaging and Deorbit using an Electrodynamic Tether.
      The satellite’s lifetime is six months after which it will be deorbited by the Electrodynamic Tether.
    </p>
    <h2>Mission and Vision</h2>
    <div class="below_text"></div>
    <p>
      <ul>
        <li class="subs_list">
          To become individuals of high caliber to meet the challenges of the country's space research program.
        </li>
        <li class="subs_list">
          To learn about the advancements in space technology and space system engineering
        </li>
      </ul>
    </p>
    <h2>Objectives</h2>
    <div class="below_text"></div>
    <p>
      <ul>
        <li class="subs_list">
          Collaborative student effort.
        </li>
        <li class="subs_list">
          To increase the student industry link.
        </li>
        <li class="subs_list">
          Contribute to the country's research program.
        </li>
        <li class="subs_list">
          Make Manipal University a respected centre for space technology
        </li>
      </ul>
    </p>
  </div>

  <?php
    require_once("footer.php");
  ?>
  <script type="text/javascript" src="js/jquery-parallax.js"></script>
  <script type="text/javascript" src="js/TweenMax.min.js"></script>
  <script type="text/javascript" src="js/hide_header.js"></script>
  <script type="text/javascript">
    // $fn.parallax( resistance, mouse )
    $( document ).mousemove( function( e ) {
      $( '#starryskyimg' ).parallax( 15, e );
      //$( '.parikshit-logo' ).parallax( 1 , e );
    });
  </script>

</body>
</html>
