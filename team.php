<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Our Team | Parikshit</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <script>



  </script>

</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  
  
  <?php
    require_once("header.php"); 
  ?>

  <div id="neeche">
    <center>
      <div>
        <h1>Our Team</h1>
      </div>
    </center>
    <center>
        <h2>System Engineers</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div">
          <img src="uploads/systemeng/AP.jpg">
          <center><h3>Akash Paliya</h3></center>
          <center><h4>Structures</h4></center>
        </div>
        <div class="team_div">
          <img src="uploads/systemeng/DS.jpg">
          <center><h3>Dhananjay Sahoo</h3></center>
          <center><h4>Payload</h4></center>
        </div>
        <div class="team_div">
          <img src="uploads/systemeng/VT.jpg">
          <center><h3>Varun Thakurta</h3></center>
          <center><h4>EPS</h4></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>Altitude Determination and Control Subsytem(ADCS)</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/ADCS/Revathi Ravula.jpg">
          <center><h4>Revathi Ravula</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ADCS/JO.JPG">
          <center><h4>Jyotirmaya Ojha</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ADCS/N Sai Krishna.jpg">
          <center><h4>N Sai Krishna</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ADCS/Naman Saxena.jpg">
          <center><h4>Naman Saxena</h4></center>
          <center><h5>Team Member</h5></center>
        </div>        
        <div class="team_div_subs">
          <img src="uploads/ADCS/Nilesh pincha.jpg">
          <center><h4>Nilesh pincha</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>Communications and Ground Station Subsytem(COMMS)</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/COMMS/Shalini Srinivasan.jpg">
          <center><h4>Shalini Srinivasan</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/COMMS/Bhagath Ch.JPG">
          <center><h4>Bhagath Ch</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/COMMS/Kshitij Sandeep.jpg">
          <center><h4>Kshitij Sandeep</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/COMMS/Nirav Annavarapu.jpg">
          <center><h4>Nirav Annavarapu</h4></center>
          <center><h5>Team Member</h5></center>
        </div>        
        <div class="team_div_subs">
          <img src="uploads/COMMS/Pranjal Paliwal.jpg">
          <center><h4>Pranjal Paliwal</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/COMMS/Raunak Hosangadi.jpg">
          <center><h4>Raunak Hosangadi</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>Electrical Power Subsytem(EPS)</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/systemeng/VT.jpg">
          <center><h4>Varun Thakurta</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/EPS/Avi Jain.JPG">
          <center><h4>Avi Jain</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>On Board Data Handling Subsytem(ODHS)</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/ODHS/Deigant Yadava.jpg">
          <center><h4>Deigant Yadava</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ODHS/Syed Afsahul Haque.JPG">
          <center><h4>Syed Afsahul Haque</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ODHS/Aditya Gupta.jpg">
          <center><h4>Aditya Gupta</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/ODHS/Akshat Vora.jpg">
          <center><h4>Akshat Vora</h4></center>
          <center><h5>Team Member</h5></center>
        </div>        
        <div class="team_div_subs">
          <img src="uploads/ODHS/Alakh Sethi.jpg">
          <center><h4>Alakh Sethi</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>Payload</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/systemeng/DS.jpg">
          <center><h4>Varun Thakurta</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/payload/Jhignas Bhogadi.JPG">
          <center><h4>Jhignas Bhogadi</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
    <hr>
    <center>
        <h2>Structures, Thermal and Mechanics SubSystem(STMS)</h2>
    </center>
    <center>
      <div class="subsystem">
        <div class="team_div_subs">
          <img src="uploads/STMS/Hemant Ganti.jpg">
          <center><h4>Hemant Ganti</h4></center>
          <center><h5>SubSystem Head</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/STMS/Akash Paliya.JPG">
          <center><h4>Akash Paliya</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/STMS/Rohan Sonkusare.jpg">
          <center><h4>Rohan Sonkusare</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/STMS/Anirudh Kailaje.jpg">
          <center><h4>Anirudh Kailaje</h4></center>
          <center><h5>Team Member</h5></center>
        </div>        
        <div class="team_div_subs">
          <img src="uploads/STMS/Athrava Tikle.jpg">
          <center><h4>Athrava Tikle</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
        <div class="team_div_subs">
          <img src="uploads/STMS/Aniketh Ajay Kumar.jpg">
          <center><h4>Aniketh Ajay Kumar</h4></center>
          <center><h5>Team Member</h5></center>
        </div>
      </div>
    </center>
  </div>

  <?php
    require("footer.php");
  ?>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
