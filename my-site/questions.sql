-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2016 at 01:17 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quizdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL,
  `choice1` varchar(150) NOT NULL,
  `choice2` varchar(150) NOT NULL,
  `choice3` varchar(150) NOT NULL,
  `answer` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=126 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `choice1`, `choice2`, `choice3`, `answer`, `url`) VALUES
(1, 'One of the following objectives is not achieved by the process of annealing', 'to soften the metal', 'to relieve internal stress', 'to refine grain structure', 'to increase the yield point', ''),
(2, 'The melting point of the filler material in brazing should be above', ' 800 deg C', ' 1000 deg C', '600 deg C', ' 420 deg C', ''),
(3, 'The mechanical advantage of a lifting machine is the ratio of', 'distance moved by effort to the distance moved by load', 'output to the input', 'all of the above', 'load lifted to the effort applied', ''),
(4, 'One of the following function is not performed by coating on the welding electrodes', 'provide protective atmosphere', 'refuce oxidation', 'stabilize the arc', 'increase the cooling rate', ''),
(5, 'The friction experienced by a body, when in motion, is known as', 'limiting friction', 'rolling friction', 'static friction', 'dynamic friction', ''),
(6, 'In a process chart the inverted triangle symbol indicates', 'inspection', 'an operation', 'transport', 'storage', ''),
(7, 'If the resultant of two equal forces has the same magnitude as either of the forces, then\r\n\r\nthe angle between the two forces is', '60°', '30°', '90°', '120°', ''),
(8, 'In oxacetylane welding, acetylene is produced using', 'Ca (OH)2 and CO2', 'CH4 and H2SO4', 'CaCO3 and H2O', 'Ca C2 and H2O', ''),
(9, 'The range of projectile on a downward inclined plane is __________ the range on\r\n\r\nupward inclined plane for the same velocity of projection and angle', 'less than', 'equal to', 'none of these', 'more than', ''),
(10, 'The force required to move the body up the plane will be minimum if it makes an angle\r\n\r\nwith the inclined plane __________ the angle of friction.', 'greater than', 'less than', 'none of these', 'equal to', ''),
(11, 'The range of a projectile is maximum, when the angle of projection is', '90°', '30°', '60°', '45°', ''),
(12, 'Two balls of equal mass and of perfectly elastic material are lying on the floor. One of\r\n\r\nthe ball with velocity v is made to struck the second ball', 'v', 'v/8', 'v/4', 'v/2', ''),
(13, 'Static friction is always __________ dynamic friction', 'equal to', 'less than', 'none of these', 'greater than', ''),
(14, 'two like parallel forces are acting at a distance of 24 mm apart and their resultant is 20\r\n\r\nN. It the line of action of the resultant is 6 mm from any given force, the two forces are', '20 N and 5 N', '15 N and 15 N', 'none of these', '15 N and 5 N', ''),
(15, 'A body will begin to move down an inclined plane if the angle of inclination of the plane\r\n\r\nis __________ the angle of friction.', 'less than', 'none of these', 'equal to', 'greater than', ''),
(16, 'One watt is equal to', '0.1 joule/s', '100 joules/s', '10 joules/s\r\n\r\n', '1 joule/s\r\n', ''),
(17, 'The maximum frictional force, which comes into play, when a body just begins to slide over the surface of the other body, is known as\r\n', 'dynamic friction\r\n', 'static friction\r\n', 'coefficient of friction\r\n', 'limiting friction', ''),
(18, 'When a rigid body is suspended vertically, and it oscillates with a small amplitude under the action of the force of gravity, the body is known as\r\n', 'second''s pendulum', 'torsional pendulum', 'simple pendulum\r\n', 'compound pendulum', ''),
(19, '. The algebraic sum of the resolved parts of a number of forces in a given direction is equal to the resolved part of their resultant in the same direction. This is known as', 'principle of independence of forces\r\n', 'principle of transmissibility of forces\r\n', 'none of these\r\n', 'principle of resolution of forces', ''),
(20, 'The law of motion involved in the recoil of gun is', 'none of these', 'Newton''s second law of motion\r\n', 'Newton''s first law of motion', 'Newton''s third law of motion', ''),
(21, 'The force, by which the body is attracted, towards the centre of the earth, is called', 'impulsive force', 'momentum\r\n', 'mass\r\n', 'weight\r\n', ''),
(22, 'Three forces acting on a rigid body are represented in magnitude, direction and line of action by the three sides of a triangle taken in order. The forces are equivalent to a couple whose moment is equal to', 'area of the triangle\r\n', 'area of the triangle\r\n', 'half the area of the triangle', 'twice the area of the triangle\r\n', ''),
(23, 'If the number of pulleys in a system is equal to its velocity ratio, then it is a __________ system of pulleys\r\n', 'third\r\n', 'first\r\n', 'fourth\r\n', 'second\r\n', ''),
(24, 'Coplaner concurrent forces are those forces which', 'do not meet at one point and their lines of action do not lie on the same plane\r\n', 'meet at one point, but their lines of action do not lie on the same plane', 'do not meet at one point, but their lines of action lie on the same plane', 'meet at one point and their lines of action also lie on the same plane\r\n', ''),
(25, 'The angle of inclination of the plane at which the body begins to move down the plane, is called\r\n', 'angle of repose', 'none of these', 'angle of projection', 'angle of friction', ''),
(26, 'The centre of gravity of a quadrant of a circle lies along its central radius (r) at a distance of\r\n', '0.5r', '0.8r', '0.7r', '0.6r\r\n', ''),
(27, 'A lead ball with a certain velocity is made to strike a wall, it falls down, but rubber ball of same mass and with same velocity strikes the same wall, it rebounds. Select the correct reason from the following:\r\n', 'both the balls undergo an equal change in momentum', 'none of the above', 'the change in momentum suffered by rubber ball is less than the lead ball\r\n\r\n', 'the change in momentum suffered by rubber ball is more than the lead ball\r\n', ''),
(28, 'In a single threaded worm and worm wheel, the number of teeth on the worm is 50. The diameter of the effort wheel is 100 mm and that of load drum is 50 mm. The velocity ratio is\r\n', '50\r\n', '150\r\n', '200\r\n', '100\r\n', ''),
(29, 'When a body falls freely under gravitational force, it possesses __________ weight.\r\n', 'maximum\r\n', 'less', 'minimum\r\n', 'no', ''),
(30, 'Which of the following statement is correct?\r\n', 'The velocity of the particle moving with simple harmonic motion is zero at the mean position', 'The periodic time of a particle moving with simple harmonic motion is directly proportional to its angular velocity', 'The acceleration of the particle moving with simple harmonic motion is maximum at the mean position.\r\n', 'The periodic time of a particle moving with simple harmonic motion is the time taken by a particle for one complete oscillation,\r\n', ''),
(31, 'According to lami’s theorem', 'the three forces must be at 120° to each other', 'the three forces must be equal', 'the three forces must be in equilibrium\r\n', 'if the three forces acting at a point are in equilibrium, then each force is proportional to the sine of the angle between the other two\r\n', ''),
(32, 'Moment of inertia of a triangular section of base (b) and height (h) about an axis passing through its vertex and parallel to the base, is __________ than that passing through its C.G. and parallel to the base.\r\n', 'nine times\r\n', 'ten times\r\n', 'seven times\r\n', 'six times', ''),
(33, 'A machine having an efficiency greater than 50%, is known as', 'non-reversible machine', 'ideal machine', 'neither reversible nor non-reversible machine', 'reversible machine', ''),
(34, 'A cycle consisting of one constant pressure, one constant volume and two isentropic processes is known as', 'Carnot cycle', 'Stirling cycle', 'Otto cycle', 'Diesel cycle', ''),
(35, 'The efficiency and work ratio of a simple gas turbine cycle are\r\n', 'low', 'High', 'very high', 'very low', ''),
(36, 'The compression ratio for petrol engines is\r\n', '3 to 6', '15 to 20', '20 to 30', '5 to 8', ''),
(37, 'The distillation carried out in such a way that the liquid with the lowest boiling point is first evaporated and recondensed, then the liquid with the next higher boiling point is then evaporated and recondensed, and so on until all the available liquid fuels are separately recovered in the sequence of their boiling points. Such a process is called\r\n', 'cracking', 'carbonisation', 'full distillation', 'fractional distillation', ''),
(38, 'One kg of carbon monoxide requires __________ kg of oxygen to produce 11/7 kg of carbon dioxide gas', 'All', '11/4', '9/7', '4/7', ''),
(39, 'A vertical column has two moments of inertia (i.e. Ixx and Iyy ). The column will tend to buckle in the direction of the', 'axis of load', 'perpendicular to the axis of load', 'maximum moment of inertia', 'minimum moment of inertia', ''),
(40, 'Euler''s formula holds good only for', 'short columns', 'both short and long columns\r\n', 'weak columns', 'long columns', ''),
(41, 'When a rectangular beam is loaded transversely, the maximum compressive stress is developed on the', 'top layer', 'neutral axis', 'every cross-section', 'bottom layer', ''),
(42, 'The point of contraflexure is a point where', 'shear force changes sign', 'shear force is maximum', 'bending moment is maximum', 'bending moment changes sign', ''),
(43, 'The energy stored in a body when strained within elastic limit is known as\r\n', 'resilience', 'proof resilience', 'impact energy', 'strain energy', ''),
(44, 'Resilience is the', 'energy stored in a body when strained within elastic limits', 'energy stored in a body when strained upto the breaking of the specimen\r\n', 'maximum strain energy which can be stored in a body', 'none of the above', ''),
(45, 'In a simple bending of beams, the stress in the beam varies\r\n', 'parabolically', 'hyperbolically', 'elliptically', 'linearly', ''),
(46, 'In free convection heat transfer transition from laminar to turbulent flow is governed by the critical value of the', 'Reynold''s number', 'Grashoff''s number', 'Reynold''s number, Grashoff''s number', 'Prandtl number, Grashoff''s number', ''),
(47, 'In a refrigerating machine, heat rejected is __________ heat absorbed.\r\n', 'equal to', 'less than', 'none of the above', 'greater than', ''),
(48, 'This stage is called:\r\n', 'Common Base\r\n', 'Common Collector', 'Emitter Follower', 'Common Emitter', 'uploads/ques_images/image1.gif'),
(49, 'A resistor with colour bands: red-red-red-gold, has the value: \r\n', '22k   5%', '220R   5% ', '22R   5%\r\n', '2k2   5%\r\n', ''),
(50, 'The resistor identified in brown is called the:\r\n', 'Base Bias Resistor', 'Emitter Feedback Resistor\r\n', 'Bypass Resistor\r\n', 'Load Resistor\r\n', 'uploads/ques_images/image3.gif'),
(51, 'Name the three leads of a common transistor\r\n', 'Collector Bias Omitter', 'Base Collector Case', 'Emitter Collector Bias', 'Collector Base Emitter', ''),
(52, 'What is the approximate characteristic voltage that develops \r\nacross a red LED?\r\n', '3.4v\r\n', '0.6v\r\n', '5v', '1.7v', ''),
(53, 'Which is not a "common" value of resistance', '2k7', '1M8\r\n', '330R', '4k4', ''),
(54, 'Two 3v batteries are connected as shown. The output voltage is: \r\n', '3v', '', '6v', '0v', 'uploads/ques_images/image4.gif'),
(55, 'The closest value for this combination is: \r\n', '4k7\r\n', '9k4\r\n', '', '2k3\r\n', 'uploads/ques_images/image5.gif'),
(56, 'the four symbols are: ', 'Electrolytic, Microphone, Resistor, Capacitor ', 'Capacitor, Piezo, Resistor, Electrolytic', 'Electrolytic, Coil, Resistor, Capacitor\r\n', 'Capacitor, Microphone, Potentiometer, Electrolytic', 'uploads/ques_images/image6.gif'),
(57, 'A resistor and capacitor in series is called a:\r\n', 'Pulse Circuit\r\n', 'Oscillator Circuit/Frequency Circuit', 'Schmitt Circuit', 'Timing Circuit/Delay Circuit\r\n', 'uploads/ques_images/image7.gif'),
(58, 'A red-red-red-gold resistor in series with an orange-orange-orange-gold resistor produces', '5k5\r\n', '55k\r\n', 'None of the above', '35,200 ohms\r\n', ''),
(59, 'A capacitor and coil in parallel is called:\r\n', 'A Timing Circuit', 'A Delay Circuit', 'A Schmitt Circuit\r\n', 'A Tuned Circuit', 'uploads/ques_images/image8.gif'),
(60, 'Identify the correctly connected LED:\r\n', 'A', 'C\r\n', 'D', 'B', 'uploads/ques_images/image9.gif'),
(61, 'The tolerance bands:   gold;  silver;  brown,  represent:\r\n', '10%,   5%,   1%', '5%, 10%,  2%\r\n', '10%,   5%,   2%', '5%,   10%,    1%\r\n', ''),
(62, 'The purpose of the capacitor', 'To pass AC on the input to the base', 'Block DC from the input line', 'To allow the stage to operate', 'To allow the transistor to self-bias', 'uploads/ques_images/image10.gif'),
(63, 'The output of a gas turbine is 300 KW and its efficiency is 20 percent, the heat supplied is\r\n', '6000 KW\r\n', '150 KW\r\n', '600 KW\r\n', '15 KW\r\n', ''),
(64, 'Reducing flame is obtained in oxyactetylane welding with', 'excess oxygen', 'equal parts of both gases\r\n', 'reduced acetylene', 'excess of acetylene', ''),
(65, 'The pump with a low initial cost and low maintenance cost is', 'double acting piston pump\r\n     ', 'gear pump', 'bucket pump\r\n', 'centrifugal pump', ''),
(66, 'A refigerant used in the domestic refrigerator is\r\n', 'methane\r\n', 'air', 'Sulphur dioxide', 'freon\r\n', ''),
(67, 'Mandrels are used to hold\r\n', 'face plate', 'cutting tools', 'drill bits', 'hollow work pieces', ''),
(68, 'An aircraft gas turbine operates on', 'Sterling cycle', 'Rankine cycle', 'Otto cycle', 'Bryton cycle\r\n', ''),
(69, 'Draft on a pattern is provided for\r\n', 'facilitating withdrawal of the pattern from the mould\r\n', 'easy lifting of the casting', 'facilitating pattern making', 'providing for strinkage of the casting\r\n', ''),
(70, 'One of the following methods is adopted for governing of steam turbines in a power plant\r\n', 'speed control', 'blow off in boiler', 'hit and miss governing', 'throttle governing\r\n', ''),
(71, 'Sprue is the passing provided for the', 'outflow\r\n', 'smooth flow', 'solidification of the molten material\r\n', 'inflow', ''),
(72, 'The operation of enlarging of a hole is called', 'drilling\r\n', 'reaming\r\n', 'counter sinking\r\n', 'boring\r\n', ''),
(73, 'Quick return motion is used in a\r\n', 'drilling\r\n', 'grinder', 'lathe\r\n', 'shaper\r\n', ''),
(74, 'For the purpose of comparison, the steam generating capacity of a boiler is generally expressed in terms of', 'kg/hr', 'steam pressure', 'thermal efficiency', 'equivalent evaporation\r\n', ''),
(75, 'Which of the following operations cannot be performed on a lathe\r\n', 'thread cutting', 'drilling', 'facing\r\n', 'slotting\r\n', ''),
(76, 'Feed in a lathe is expressed in\r\n', 'mm', 'mm per degree', 'rpm', 'mm per revolution', ''),
(77, 'Rapping allowance is provided on a pattern to take care of\r\n', 'distortion\r\n', 'machining\r\n', 'shrinkage\r\n', 'easy withdrawl', ''),
(78, 'Brasses and bronzes are welded using', 'reducing flame', 'carburising flame', 'oxidizing flames', 'neutral flame', ''),
(79, 'Which of the following is not used as a refrigerant ?\r\n', 'carbon dioxide', 'amonia', 'sulphur dioxide', 'carbon monoxide', ''),
(80, 'In Industry of the developed nations the type of production most often occurring is\r\n', 'Create lot production', 'Single lot production', 'Mass production', 'Batch production', ''),
(81, 'what are wobble shimmy vibrations?', 'steering vibrations ', 'vibrations caused due to imbalanced or worn parts', 'none of the above', 'Both a &b', ''),
(82, 'what is the difference between pressure and stress?', 'internal &external', 'both external', 'both internal', 'external & internal', ''),
(83, 'Find Vo.(See the figure)', '6V ', '27V', '-18V', '12V', 'uploads/ques_images/image11.jpg'),
(84, 'Ratio of lateral strain to linear strain is called', 'Bulk''s Modulus ', 'Young''s Modulus', 'Modulus of rigidity', 'Poisson''s Ratio', ''),
(85, 'Excess-3 conversion of binary 6 is', '1000 ', '1010 ', '1011 ', '1001', ''),
(86, 'Which of the following will have more strength if material, weight and length are same?', 'Solid shaft', 'Both will be equal', 'Can''t determine', 'Hollow shaft', ''),
(87, 'if a material has no uniform density throughout the body, then the position of centroid and center of mass are', 'identical', 'independent upon the density ', 'unpredictable', 'not identical ', ''),
(88, 'The gray code of the binary code 110', ' 001', '111', '110', '101', ''),
(89, 'Compression ratio of 4 stroke petrol             engine is', '7:2 to 9:20', '9:4 to 10:15', '15:1 to 22:1', '7:1 to 9:1', ''),
(90, 'Babcock & wilcox boiler is which type ?', 'fire tube boiler', 'liquid fueled ', 'Natural circulation boiler', ' water tube boiler', ''),
(91, 'When the diode is in forward bias, it is equivalent to', 'An OFF switch', 'A high resistor', 'A very low resistance ', 'An ON switch', ''),
(92, 'Solve AB + A''C'' + AB''C(AB+C)', '0', 'A', 'AB''C (AC+B)', '1', ''),
(93, 'A basic S-R flip-flop can be constructed by cross-coupling which basic logic gates ?', 'XOR or XNOR GATES', 'AND or NOR GATES', 'AND or OR GATES', 'NOR or NAND GATES', ''),
(94, 'A colour code of orange, orange, orange is for what ohmic value ?', '3300 ohms', '44000 ohms', '22 kilo ohms', '33 kilo ohms', ''),
(95, 'Work done in a free expansion process is', 'Maximum', 'Minimum', 'Positive', 'Zero', ''),
(96, 'In ideal machines, mechanical advantage is _________ velocity ratio.', 'None of these', 'Less than', 'Greater than', 'Equal to', ''),
(97, 'The center frequency of a band pass filter is always equal to', '-3 dB frequency', 'Bandwidth', 'Bandwidth divided by Q', 'Geometrical average of the critical frequencies.', ''),
(98, 'At a given instant ship A is travelling at 6km//hr due east and ship B is\r\ntravelling at 8km/hr due north. The velocity of relative to A is', '7km/hr', '14 km/hr', '1 km/hr', '10 km/hr', ''),
(99, 'In an amplifier with negative feedback', 'Only the gain of the amplifier is affected', 'Only the gain and the bandwidth of the amplifier is affected', 'Only the input and output impedances are affected', 'All the four parameters mentioned above are affected.', ''),
(100, 'The co-efficient of friction depends upon', 'Area of contact', 'Shape of the surface', 'All of the above', 'Nature of surfaces', ''),
(101, 'The ideal op-amp has the following characteristics', 'Ri=0,A=infinity,Ro=0', 'Ri=infinity,A=infinity,Ro=infinity', 'Ri=0,A=infinity,Ro=infinity', 'Ri=infinity,A=infinity,Ro=0', ''),
(102, 'The units of moment of inertia of an area are', 'Kg-m^2', 'm^4', 'kg/m^2', 'kg/m^2', ''),
(103, 'The field at any point on the axis of a current carrying coil will be', 'parallel to axis', 'perpendicular to the axis.', 'zero.', 'at angle of 45° with the axis.', ''),
(104, ' Which field is associated with the capacitor?', 'Magnetic', 'Both of (A) and (B)', 'None of above.', 'Electric.', ''),
(105, 'A DC generator without commutator is a', 'DC motor', 'DC generator\r\n', 'induction motor', 'AC generator', ''),
(106, 'How many bits are required to store one BCD digit ?', '1', '2', '3', '4', ''),
(107, 'The effective inhibitor of pre-ignition is', 'alcohol', 'none of these', 'lead', 'water', ''),
(108, 'Function of transducer is to convert', 'Electrical signal into non electrical quantity', 'Electrical signal into mechanical quantity', 'All of these', 'Non electrical quantity into electrical signal', ''),
(109, 'An OR gate has 6 inputs ,the number of input combinations in its truth table –', '6', '32', '128', '64', ''),
(110, 'Digital signals deal in the realm of the _______ possibilities', 'continous', 'infinite', 'discontinous', 'finite', ''),
(111, 'Digital circuits operate using __________ signals', 'both continous and discontinuos', 'continous', 'none', 'discontinous', ''),
(112, 'Cellphone chargers are -', 'DC to AC converters', 'none of the above', 'both', 'AC to DC converters', ''),
(113, 'Difference between a generator and a motor is that –', 'both convert electrical to mechanical', 'latter one doesn’t work on electromangnetic induction', 'none of the abaove', 'latter converts electrical energy to mechanical and former converts mechanical to electrical', ''),
(114, 'Measured Voltage V= 20 +0.02ohm .Find the relative error?', '3%', '6.90%', '0.01%', '0.1%', ''),
(115, 'metrology is the science of-', 'environmental changes', 'weather conditions', 'none of the above', 'measurement', ''),
(116, 'Consider a string of Christmas lights . box 1 contains\r\nexpensive pack of led lights connected in series ; box 2\r\ncontains cheap pack of led lights connected in parallel-\r\nWhich is better', 'box 1', 'both 1 and 2', 'none', 'box 2', ''),
(117, 'consider two different metals joined together forming a closed cloop\r\nhaving two different temperature junctions , what will the effect-', 'current doesn’t flow', 'none', 'can not be determined', 'current flows', ''),
(121, 'Siemens or Mho  is the unit of ', 'Conductance', 'Admittance', 'None of the above', 'Both', ''),
(122, 'If you have two different bulbs in series (e.g. a round bulb and a\r\nlong bulb). Round bulb has a resistance of 25.678 ohm and long bulb\r\nhas resistance of 27 ohm . which bulb will glow brighter?', 'both will glow equally', 'round bulb', 'none', 'long bulb', ''),
(123, 'In a lifting machine, a load of 240 N is being raised and the velocity ratio is 20. If the efficiency of the machine is 40%, the effort is equal to', '35', '25', '20', '30', ''),
(124, 'The common collector amplifier is also known as \r\n', 'Collector follower', 'Base follower ', 'Source follower', 'Emitter follower ', ''),
(125, 'A Constant load power means a uniform conversion of', 'Mechanical energy to electrical energy', 'current to voltage', 'voltage to current\r\n', 'electrical energy to mechanical energy', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
