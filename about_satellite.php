<!DOCTYPE html>
<html>
<head>
	<title>Insert title here</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>About Us | Parikshit</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="uploads/favicon.png">

  <script type="text/javascript" src="js/vendor/autosize.min.js"></script>
  <script type="text/javascript" src="js/vendor/modernizr-2.8.3.min.js"></script>-->

</head>
<body>

 <?php
    require_once("header.php"); 
 ?>
 <div id="neeche">
 	<div id="image-container">
 		<img src="uploads/logo1.png">
 	</div>
 	<div id="floated">
 		<p>
		  Parikshit is the first 2U class Nano satellite project under taken by the students of Manipal University.
      Nanosatellites, also called ‘NanoSats’, is a recently coined term used to describe artificial satellites with mass between 1 and 10 kilograms. Nanosatellites are built with units of CubeSat, which is a volume of exactly one liter (10 cubic cm) and a mass not more than 1.33 kilograms.
      Brief specification of the satellite are as follow:
      Size of satellite -10x10x22.7 cm.
      Mass -2.30 kg.
      Speed-7.2 km/s (approx.).
      Mission Life- 6 months.
      Payload :
      Terrestrial thermal imaging.
      Deorbiting using Electro-dynamic tether.
      Camera range-130 horizontal, 260 vertical.
      Preferred Orbit :
      10:30 AM Polar Sun synchronous orbit.
      Altitude - 850 km(approx.).
      Inclination : 97.89 degree.
      Solar panel : Body mount solar panels on 3 sides.
      Dual microcontroller based hardware architecture running Micrium RTOS.
      Antennas : Two deployable antennas
      Dipole antenna for payload data at 437.8 MHz.
      Half duplexed monopole antenna for time divided multiple access between uplink and  beacon at 145.89 MHz.
		</p>
</body>
</html>