<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Contact Us | Parikshit</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <script>

    function changeClass(event)
    {
      var target = event.target || event.srcElement;
      if(window.location.href == event.target)
      {
        var ul = document.getElementsByClassName("drop_menu");
        for(var i = 0; i < ul.length; i++)
        {
        }
      }

    }

  </script>

</head>
<body>

  <!-- Contact Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  
  
  <?php
    require_once("header.php"); 
  ?>

  <div id="cont">
    <div id="left_div">
      <div id="contact_heading">
        <h2>Contact us.</h2>
      </div>
      <form> 
        <table id="contact_table">   
          <tr>   
            <div id="name_div">
              <td><img src="uploads/icon_name.png"></img></td>
              <td><input type="text" placeholder="Name"></td>
            </div>
          </tr>
          <tr>
            <div id="email_div">
              <td><img src="uploads/email-icon.png"></img></td>
              <td><input type="email" placeholder="Email"></td>
            </div> 
          </tr>
          <tr>
            <div id="message_div">
              <td><img src="uploads/icon-pencil.png"></img></td>
              <td><textarea placeholder="Message" rows="4" cols="30"></textarea></td>
          </div>
        </tr>
        <tr>
          <div id="button_div">
            <td></td>
            <td><input type="submit" placeholder="Email" value="Submit"></td>
          </div>
        </tr>
        </table>
      </form>
    </div>

    <div id="right_div">
      <p>
        Hello. This is Rachit and Soham.         
      </p>
      <img src="uploads/logo1.png">
    </div>
  </div>

  <?php
    require("footer.php");
  ?>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
