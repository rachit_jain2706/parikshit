<!DOCTYPE html>
<html>
<head>
	<title>Subsystems | Parikshit</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Subsytems | Parikshit</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="Skeleton/css/normalize.css">
  <link rel="stylesheet" href="Skeleton/css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="uploads/icon-pencil.png">

</head>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		function goToByScroll(id){
	    $('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
	}
	</script>
<body>

<?php
	require_once("header.php"); 
?>
	<div id="neeche">		
	    <center>
	      <div>
	      	<div class="row">
		        <div class="col-md-4 span4" onclick="goToByScroll()">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/adcs.png"></a>
		          <div class="overlay">		          
		          	<h1>ADCS</h1>
		          </div>
		        </div>
		        <div class="col-md-4 span4">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/comms.png"></a>
		          <div class="overlay">		          
		          	<h1>COMMS</h1>
		          </div>
		        </div>
		        <div class="col-md-4 span4">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/eps.png"></a>
		          <div class="overlay">		          
		          	<h1>EPS</h1>
		          </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-4 span4">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/odhs.png"></a>
		          <div class="overlay">		          
		          	<h1>ODHS</h1>
		          </div>
		        </div>
		        <div class="col-md-4 span4">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/payload.png"></a>
		          <div class="overlay">		          
		          	<h1>Payload</h1>
		          </div>
		        </div>
		        <div class="col-md-4 span4">
		          <a href=""><img class="grid_images" src="uploads/phone-icon/structures.png"></a>
		          <div class="overlay">		          
		          	<h1>STMS</h1>
		          </div>
		        </div>
		    </div>
	      </div>
	    </center>
	    <hr>

	    <div>
    		<center><h2>Altitude Determination and Control Subsytem(ADCS)</h2></center>
    	</div>
	    <div class="subs_div_right">	    	
	    	<div class="img_div_right">
	    		<img src="uploads/subsystempage/comms.png">
	    	</div>
	    	<div id="adcs">
    			Attitude Determination and Control Subsystem or ADCS is responsible for the orientation and control of the Satellite.
			    It has to exercise control over the orientation of the satellite by having the required accuracy and meeting other constraints like power and computational complexity.
			    This is necessary to point the onboard payloads at the right direction and
				transmit the payload data over the ground station accurately ensure a stable satellite.
			    The determination of current orientation is done with the help of sensors and algorithms and the control is done using the actuators. The determination of attitude is done with the help of suitable hardware from which data is fed into algorithms.
			    The on-board orbit propagation hardware and algorithms are used to propagate the GPS data to get the real time position and velocity of the Satellite. The Control System then accepts the inputs from the estimation algorithms regarding the error in the orientation.<br>
			    The control system commands the actuators to perform accordingly to orient the satellite in the required orientation.
			    The stability is maintained and if the angular rates increase the actuators act again making the satellite stable.
	    	</div>
	    </div>
	    <hr>

	    <div id="comms">
    		<center><h2>Communications and Ground Station Subsytem(COMMS)</h2></center>
    	</div>
	    <div class="subs_div_left">	    	
	    	<div>
    			The Communications and Ground Station System, or COMMS, is responsible for the flow of information to and fro from the satellite.
    			<br><center><u>Communication</u></center>
    			<br><br>
    			There are three paths of communication that we facilitate
			    <ul>
			    <li class="subs_list">The beacon downlink, which carries the name, call sign and health-monitoring data of the satellite.</li>
			    <li class="subs_list">The uplink, which conveys all necessary telecommands to the satellite</li>
			    <li class="subs_list">The payload downlink, which sends data from the satellite, namely thermal images captured by it.</li> 
			    </ul>
			    The subsequent communication onboard the satellite is achieved by configuring transmitter and receiver IC’s via the coding of microcontrollers. This also includes the communication between the pcbs on board the satellite. This is achieved by various protocols that help in the transfer of data across the satellite.
			    <br><br><center><u>Ground Station</u><br></center><br>
			    <ul>
			    <li class="subs_list">For the purpose of establishing and maintaining communication with the satellite, we have a fully functional ground station set up in MIT that establishes communication via two RF antenna, one for UHF band and the other for VHF frequency band.</li> 
			    <li class="subs_list">PAGOS, or the Parikshit Ground station, is capable of not only establishing contact with our satellite, but also has functionalities that enable it to track and receive beacon data from any currently operating satellite.</li>
			    <li class="subs_list">Permissions granted to us by the International Association of Research Universities allows us to communicate with any amateur radio operator in the world via the ground station.</li>
			    </ul>
	    	</div>
	    	<div class="img_div_left">
	    		<img src="uploads/subsystempage/comms.png">
	    	</div>
	    </div>
	    <hr>

	    <div id="eps">
    		<center><h2>Electrical Power Subsytem(EPS)</h2></center>
    	</div>
	    <div class="subs_div_right">	    	
	    	<div class="img_div_right">
	    		<img src="uploads/subsystempage/comms.png">
	    	</div>
	    	<div>
    			The Electrical Power Subsystem is responsible for harnessing, conditioning, storing and distributing power on-board the satellite.
    			<ul>
    				<li class="subs_list">Solar cells with 26.8% BOL efficiency</li>
    				<li class="subs_list">Battery management system</li>
    				<li class="subs_list">Bus voltage regulation</li>
    				<li class="subs_list">Protection against over-voltage, over-current and under-voltage faults</li>
    				<li class="subs_list">Protection against latch-ups</li>
    			</ul>
    			The Electrical Power Subsystem also deals with the design and soldering of the PCBs that we use for testing. All the PCBs that we use are designed and soldered in-house.
	    	</div>
	    </div>
	    <hr>

	    <div id="odhs">
    		<center><h2>On Board Data Handling Subsytem(ODHS)</h2></center>
    	</div>
	    <div class="subs_div_left">	    	
	    	<div>
    			Any satellite system requires a large amount of data to be acquired and processed which is basically the work done by the On-board Data Handling Subsystem. The subsystem deals with a variety of tasks that include working with both hardware as well as software. The subsystem also works on the various algorithms that are required to effectively process and use the acquired data and also ensure that the tasks of the satellite run in an ordered manner.<br>
					Brief specification are as follow :
					Dual microcontroller based hardware architecture is used.
					The primary board has STM32F2xx as its microcontroller.
					 The secondary board has MSP430xx as its microcontroller.
					I2C, SPI and UART is used for communication between hardware.
					Micrium OS is used to handle tasks.
					Real time operating system (RTOS) is implemented.<br>
					The ODHS subsystem handles all the non-trivial tasks which include<br>
					Scheduling these tasks of the system using a round robin scheduler.<br>
					Regulation of housekeeping data and Beacon health data.<br>
					Time Synchronization of specific scheduled tasks as per the constraints of position, orbit and time.<br>
					The subsystem makes sure that all parts of the satellite work together faultlessly and also ensures that the satellite is able to sustain itself throughout its lifetime without any major external inputs.
	    	</div>
	    	<div class="img_div_left">
	    		<img src="uploads/subsystempage/odhs.jpg">
	    	</div>
	    </div>
	    <hr>

	    <div id="payload">
    		<center><h2>Payload</h2></center>
    	</div>
	    <div class="subs_div_right">	    	
	    	<div class="img_div_right">
	    		<img src="uploads/subsystempage/comms.png">
	    	</div>
	    	<div>
    			 For a satellite payload is the devices that will help accomplish the mission of the satellite.p
			      Parikshit has two payloads
			      <ol>
			        <li> the Thermal infrared camera, that will take images of the Indian subcontinent and monitor
			         <ol>
			          <li>
			          Urban Heat Islands(UHI),a city or metropolitan area that  is significantly warmer than its surrounding rural areas due to human activities.
			          </li>
			          <li>
			          ocean monitoring.<br>
			           a)variation in sea surface temperature<br>
			           b)monitoring of ocean currents<br>
			           c) diurnal temperature differences<br>
			          </li>
			          <li><br>
			            cloud monitoring
			          </li>
			        </ol>
			      A better understanding of the role of clouds in climate has been identified as a potential feedback effect of clouds,which is a major source of uncertainty in predictions of greenhouse warming. The main aim here is to obtain the thermal distribution of the clouds and then differentiate them based on the distribution. Cyclones would be of particular interest because of their size and behaviour.
			      </li>
			      <li>
			        Electrodynamic tether, which will be used for deorbiting of the satellite after its life. We have collaborated with an Austrailian company, Saber Astronautics. This is an innovative payload, as this has never been used before for deorbiting nano-satellites. This is one of our missions that we have undertaken to make sure that the atmosphere of Earth is not riddled with our space debris. 
		      		</li>
				</ol>
	    	</div>
	    </div>
	    <hr>

	    <div id="stms">
    		<center><h2>Structures, Thermal and Mechanics SubSystem(STMS)</h2></center>
    	</div>
	    <div class="subs_div_left">	    	
	    	<div>
    			The structures, thermal and mechanisms subsystem is responsible for designing, fabricating and assembling the final structure of the satellite. While doing so, the subsystem ensures that the structure can withstand all launch loads and vibrations, as well as exposure to space radiation during the life of the mission.
				The primary objectives of the subsystem are:
				<ul>
					<li class="subs_list">Design the satellite keeping in mind the 2U nanosatellite constraints and ensuring  minimal wastage of available space </li>
					<li class="subs_list">To provide a structure that can endure the high accelerations, loads and vibrations that manifest during launch</li>
					<li class="subs_list">To integrate all components of other subsystems, warranting smooth interfacing between them</li>
				</ul>
				The thermals division of the STM subsystem is in charge of designing the thermal layout of the satellite. This constitutes:
				<ul>
					<li class="subs_list">Protect the sensitive internal components of the satellite from harmful space radiation and environment</li>
					<li class="subs_list">Placement and usage of thermal control elements – active, passive or both – to provide an optimum thermal circuit in order to limit internal temperatures and fluxes within the operating and survival ranges of each sensitive component</li>
				</ul>
	    	</div>
	    	<div class="img_div_left">
	    		<img src="uploads/subsystempage/comms.png">
	    	</div>
	    </div>
	</div>
	<?php
		require_once("footer.php"); 
	?>
</body>
</html>